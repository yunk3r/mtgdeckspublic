package com.example.michaelyunker.mtgdecksearch;

import android.util.Log;

import java.util.ArrayList;


public class Event {

    private String location;
    private String date;
    private String type;

    public Event(String l,String d, String t)
    {
        location=l;
        date=d;
        type=t;
    }
    public Event()
    {
    }

    public String getLocation()
    {
        return location;
    }

    public String getDate()
    {
        return date;
    }
    public String getType()
    {
        return type;
    }

    public static ArrayList<Event> clean(ArrayList<Event> list)
    {
        ArrayList<Event> temp=new ArrayList<Event>();
        temp.add(list.get(0));
        boolean f=false;
        for(int i=0;i<list.size();i++)
        {
            for(int j=0;j<temp.size();j++)
            {
                if(!(list.get(i).getLocation().equalsIgnoreCase(temp.get(j).getLocation())&&list.get(i).getDate().equals(temp.get(j).getDate())))
                {
                    f=false;
                }
                else
                {
                    f=true;
                    break;
                }


            }
            if(f==false)
            {
                temp.add(list.get(i));
            }
        }
        int low=0;
        Event t;
        for(int i=0;i<temp.size()-1;i++)
        {
            for(int j=i;j<temp.size()-1;j++)
            {
               if(temp.get(j).date.compareTo(temp.get(low).date)>0)
               {
                   low=j;
                   t=temp.get(i);
                   temp.set(i,temp.get(low));
                   temp.set(low,t);
               }
            }

        }
        return temp;
    }

}
