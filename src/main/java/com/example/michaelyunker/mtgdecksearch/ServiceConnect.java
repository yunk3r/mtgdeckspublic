package com.example.michaelyunker.mtgdecksearch;


import android.util.Log;

import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;


import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;

public class ServiceConnect {

    private String ip;
    private static  ServiceConnect service;

    private ServiceConnect(String ip)
    {
        this.ip=ip;
    }


    public static void Connect(String ip)
    {
        Log.i("IP",ip);
        service=new ServiceConnect(ip);
    }



    public static ServiceConnect Get()
    {
        if(service==null)
        {
            return null;
        }
        return service;
    }



    public ArrayList<Event> GetScgEvents(String event)
    {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        Event[] responseEntity = restTemplate.getForObject(ip+"/EventScg?event="+event, Event[].class);
        ArrayList<Event> Event=  new ArrayList<Event>();
        for(Event e : responseEntity )
        {
            Event.add(e);
        }
        return Event;
    }

    public ArrayList GetProEvents(String event)
    {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        Event[] responseEntity = restTemplate.getForObject(ip+"/EventPro", Event[].class);
        ArrayList<Event> Event=  new ArrayList<Event>();
        for(Event e : responseEntity )
        {
            Event.add(e);
        }
        return Event;
    }

    public ArrayList<Deck> GetScgDecks(String location,String date,String d)
    {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        Deck[] responseEntity = restTemplate.getForObject(ip+"/ScgDecks?Location="+location+"&Date="+date+"&d="+d, Deck[].class);
        ArrayList<Deck> decks=  new ArrayList<Deck>();
        for(Deck e : responseEntity )
        {
            decks.add(e);
        }
        return decks;
    }

    public ArrayList<Deck> GetProDecks(String location,String date)
    {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        Deck[] responseEntity = restTemplate.getForObject(ip+"/ProDecks?Location="+location+"&Date="+date, Deck[].class);
        ArrayList<Deck> decks=  new ArrayList<Deck>();
        for(Deck e : responseEntity )
        {
            decks.add(e);
        }
        return decks;
    }

    public String GetScgDeckList(int id,String d)
    {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
        String x=ip+"/ScgList?id="+id+"&d="+d;
        String responseEntity = restTemplate.getForObject(x, String.class,"android");
        
        return responseEntity;
    }

    public String GetProDeckList(int id)
    {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
        String x=ip+"/ProList?id="+id;
        String responseEntity = restTemplate.getForObject(x, String.class,"android");

        return responseEntity;
    }

    public ArrayList<Deck> Search (String format,String name, String cards)
    {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        Deck[] responseEntity = restTemplate.getForObject(ip+"/Search?format="+format+"&name="+name+"&cards="+cards, Deck[].class);
        ArrayList<Deck> decks=  new ArrayList<Deck>();
        for(Deck e : responseEntity )
        {
            decks.add(e);
        }
        return decks;


    }

}
