package com.example.michaelyunker.mtgdecksearch;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.SharedPreferences;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;


public class MainActivity extends AppCompatActivity {

    String[] menu;
    DrawerLayout dLayout;
    ListView dList;
    ArrayAdapter<String> adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        menu = new String[]{"Search","Events","About"};
        dLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        dList = (ListView) findViewById(R.id.left_drawer);
        adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,menu);
        dList.setAdapter(adapter);
        final Fragment main = new EventFragment();
        final FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_frame, main).commit();

        SharedPreferences pref = getSharedPreferences("mypref", MODE_PRIVATE);

        if(pref.getBoolean("firststart", true)){
            // update sharedpreference - another start wont be the first
            SharedPreferences.Editor editor = pref.edit();
            editor.putBoolean("firststart", false);
            editor.commit();
        dLayout.openDrawer(Gravity.LEFT);

        }
        try {

            InputStream stream = getAssets().open("IPAddress");
            int size = stream.available();
            byte[] buffer = new byte[size];
            stream.read(buffer);
            stream.close();
            String address = new String(buffer);
            ServiceConnect.Connect(address);
        }catch (Exception e)
        {
        }


        dList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View v, int position, long id) {
                dLayout.closeDrawers();
                Bundle args = new Bundle();

                              args.putString("Menu", menu[position]);
                if (menu[position] == "Search") {

                    Fragment search = new SearchFragment();
                    search.setArguments(args);
                    fragmentManager.beginTransaction().replace(R.id.content_frame, search).commit();

                } else if (menu[position] == "Events") {
                    final Fragment events = new EventFragment();
                    events.setArguments(args);
                    fragmentManager.beginTransaction().replace(R.id.content_frame, events).commit();
                } else if (menu[position] == "About") {
                    Fragment about = new About();
                    about.setArguments(args);
                    fragmentManager.beginTransaction().replace(R.id.content_frame, about).commit();
                }

            }
        });
    }


}
