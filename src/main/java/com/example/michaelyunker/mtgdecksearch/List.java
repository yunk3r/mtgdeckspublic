package com.example.michaelyunker.mtgdecksearch;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.TextView;



public class List extends Activity {


    LayoutInflater inflater;
    FrameLayout frame;
    String List="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        frame=(FrameLayout) findViewById(R.id.DeckFrame);
        inflater = this.getLayoutInflater();

        Integer []info=new Integer[2];
        info[0]=getIntent().getIntExtra("id",0);
        int type=getIntent().getIntExtra("type",0);
        if(type==1)
        {
            info[1]=1;
        }
        else if(type==2)
        {
            info[1]=2;
        }
        else if(type==3)
        {
            info[1]=3;
        }
        else if(type==4)
        {
            info[1]=4;
        }
        else if(type==5)
        {
            info[1]=5;
        }
        else if(type==6)
        {
            info[1]=6;
        }
        else if(type==7)
        {
            info[1]=7;
        }

        loadDeckList load=new loadDeckList();
        load.execute(info);

    }


    class loadDeckList extends AsyncTask<Integer,Void,String>
    {

        protected void onPreExecute ()
        {
            TextView tv=new TextView(frame.getContext());
            tv.setText("Loading Decks");
            frame.addView(tv);


        }
        @Override
        protected String doInBackground(Integer...params)
        {

            if(params[1].intValue()==1) {
                List=ServiceConnect.Get().GetScgDeckList(params[0],"scg");
            }
            else if(params[1].intValue()==2) {
                List=ServiceConnect.Get().GetScgDeckList(params[0],"scgLegacy");
            }
            else if(params[1].intValue()==3) {
                List=ServiceConnect.Get().GetScgDeckList(params[0], "scgModern");
            }
            else if(params[1].intValue()==4) {
                List=ServiceConnect.Get().GetProDeckList(params[0]);
            }
            else if(params[1].intValue()==5) {
                List=ServiceConnect.Get().GetScgDeckList(params[0], "scgStandardClassic");
            }
            else if(params[1].intValue()==6) {
                List=ServiceConnect.Get().GetScgDeckList(params[0], "scgModernClassic");
            }
            else if(params[1].intValue()==7) {
                List=ServiceConnect.Get().GetScgDeckList(params[0], "scgLegacyClassic");
            }


            return List;
        }
        @Override
        protected void onPostExecute(String result)
        {
            frame.removeAllViews();

            TextView tv=new TextView(frame.getContext());
            tv.setText(result);
            tv.setVerticalScrollBarEnabled(true);
            frame.setVerticalScrollBarEnabled(true);
            frame.addView(tv);



        }
    }
}
