package com.example.michaelyunker.mtgdecksearch;


import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;


public class SearchFragment extends Fragment {

    View view;
    LinearLayout CardContainer;
    Spinner formatSpinner;
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view =inflater.inflate(R.layout.fragment_search, container, false);
        formatSpinner=(Spinner) view.findViewById(R.id.FormatDropDown);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(view.getContext(),
                R.array.formats_array, android.R.layout.simple_spinner_item);

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        formatSpinner.setAdapter(adapter);
        Button add=(Button) view.findViewById(R.id.addButton);

        CardContainer=(LinearLayout) view.findViewById(R.id.CardConstainer);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View card=inflater.inflate(R.layout.card, CardContainer, false);

                // set the card name text
                ((TextView)card.findViewById(R.id.CardName)).setText(((TextView) view.findViewById(R.id.CardNameText)).getText());
                ((TextView) view.findViewById(R.id.CardNameText)).setText("");

                Button remove=(Button) card.findViewById(R.id.remove);
                remove.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View v) {
                        ((LinearLayout)v.getParent().getParent()).removeView((View) v.getParent());
                    }
                });

                CardContainer.addView(card);
            }
        });

        Button searchButton=(Button) view.findViewById(R.id.Search);

        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String cards="";
                for(int i=0;i<CardContainer.getChildCount();i++)
                {
                    String t=((TextView)CardContainer.getChildAt(i).findViewById(R.id.CardName)).getText().toString();
                    t.replace(" ", "+");
                    cards=cards+t+",";
                }
                cards=cards.replace("\'","\\'");
                Intent intent = new Intent(getActivity(), SearchResults.class);
                intent.putExtra("type", 5);
                intent.putExtra("cards", cards);
                intent.putExtra("format", ((Spinner) view.findViewById(R.id.FormatDropDown)).getSelectedItem().toString().toLowerCase());
                intent.putExtra("name", ((TextView) view.findViewById(R.id.DeckName)).getText().toString());
                startActivity(intent);
            }
        });

        return view;
    }
}
