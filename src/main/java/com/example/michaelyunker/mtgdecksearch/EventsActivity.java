package com.example.michaelyunker.mtgdecksearch;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;


public class EventsActivity extends Activity {

    LinearLayout Table;
    TextView Event2;
    String menu;
    ArrayList<Event> EventList;
    View view;
    LayoutInflater inflater;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_events);
        Table= (LinearLayout) findViewById(R.id.EventTable);
        inflater = this.getLayoutInflater();
        loadEvents load=new loadEvents();
        if(getIntent().getIntExtra("type",0)==1)
        {
            load.execute(1);
        }
        if(getIntent().getIntExtra("type",0)==2)
        {
            load.execute(2);
        }
        if(getIntent().getIntExtra("type",0)==3)
        {
            load.execute(3);
        }
        if(getIntent().getIntExtra("type",0)==4)
        {
            load.execute(4);
        }
        if(getIntent().getIntExtra("type",0)==5)
        {
            load.execute(5);
        }
        if(getIntent().getIntExtra("type",0)==6)
        {
            load.execute(6);
        }
        if(getIntent().getIntExtra("type",0)==7)
        {
            load.execute(7);
        }
    }

    public void loadDecks(View v)
    {

        Event t=(Event)v.getTag();
        Intent intent = new Intent(this, Decks_Activity.class);
        intent.putExtra("loc", t.getLocation());
        intent.putExtra("Date", t.getDate());
        if(t.getType().equalsIgnoreCase("starcityGames.com Standard Open"))
        {
            intent.putExtra("type",1);
        }
        else if(t.getType().equalsIgnoreCase("Pro Standard"))
        {
            intent.putExtra("type",4);
        }
        else if(t.getType().equalsIgnoreCase("starcityGames.com Legacy Open"))
        {
            intent.putExtra("type",2);
        }
        else if(t.getType().equalsIgnoreCase("starcityGames.com Modern Open"))
        {
            intent.putExtra("type",3);
        }
        else if(t.getType().equalsIgnoreCase("starcityGames.com Classic")&& getIntent().getIntExtra("type",0)==5)
        {
            intent.putExtra("type",5);
        }
        else if(t.getType().equalsIgnoreCase("starcityGames.com Classic")&& getIntent().getIntExtra("type",0)==6)
        {
            intent.putExtra("type",6);
        }
        else if(t.getType().equalsIgnoreCase("starcityGames.com Classic")&& getIntent().getIntExtra("type",0)==7)
        {
            intent.putExtra("type",7);
        }
        startActivity(intent);
    }

    class loadEvents extends AsyncTask<Integer,Void,ArrayList>
    {

        protected void onPreExecute ()
        {
            TextView tv=new TextView(Table.getContext());
            tv.setText("Loading Events");
            Table.addView(tv);


        }
        @Override
        protected ArrayList doInBackground(Integer...params)
        {
            
            EventList=new ArrayList<Event>();

            if(params[0]==1)
            {
                EventList= ServiceConnect.Get().GetScgEvents("scg");
            }
            else if(params[0]==2)
            {
                EventList= ServiceConnect.Get().GetScgEvents("scgLegacy");
            }
            else if(params[0]==3)
            {
                EventList= ServiceConnect.Get().GetScgEvents("scgModern");
            }
            else if(params[0]==4)
            {
                EventList= ServiceConnect.Get().GetProEvents("standard");
            }
            else if(params[0]==5)
            {
                EventList= ServiceConnect.Get().GetScgEvents("scgStandardClassic");
            }
            else if(params[0]==6)
            {
                EventList= ServiceConnect.Get().GetScgEvents("scgModernClassic");
            }
            else if(params[0]==7)
            {
                EventList= ServiceConnect.Get().GetScgEvents("scgLegacyClassic");
            }

            return EventList;
        }

        protected void onPostExecute(ArrayList result)
        {
            Table.removeAllViews();
            for(int i=0;i<result.size();i++)
            {
                View eventBox=inflater.inflate(R.layout.event_box, Table, false);
                eventBox.setTag(EventList.get(i));
                final TextView eventName=(TextView) eventBox.findViewById(R.id.EventName);
                final TextView eventDate=(TextView) eventBox.findViewById(R.id.EventDate);

                eventName.setText((EventList.get(i).getLocation()));
                eventDate.setText((EventList.get(i).getDate()));

                eventBox.setOnClickListener(new View.OnClickListener(){
                    public void onClick(View arg0) {
                        loadDecks(arg0);

                    }
                });

                eventBox.setOnTouchListener(new View.OnTouchListener() {

                    @Override
                    public boolean onTouch(View v, MotionEvent event) {

                        switch(event.getAction()) {

                            case MotionEvent.ACTION_DOWN:
                                v.setBackgroundColor(Color.LTGRAY);

                                break;
                            case MotionEvent.ACTION_CANCEL:

                                v.setBackgroundColor(Color.TRANSPARENT);
                                break;
                            case MotionEvent.ACTION_UP:

                                v.setBackgroundColor(Color.TRANSPARENT);
                                break;
                        }

                        return false;
                    }

                });


                Table.addView(eventBox);
            }

        }
    }



}

