package com.example.michaelyunker.mtgdecksearch;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;


public class Decks_Activity extends Activity {
    LinearLayout Table;
    LayoutInflater inflater;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_decks);
        Table= (LinearLayout) findViewById(R.id.DeckTable);
        inflater = this.getLayoutInflater();

        String loc=getIntent().getStringExtra("loc");
        String date=getIntent().getStringExtra("Date");
        String []info=new String[3];
        info[0]=loc;
        info[1]=date;
        if(getIntent().getIntExtra("type",1)==1) {
            info[2]="1";
        }
        else if(getIntent().getIntExtra("type",2)==2) {
            info[2]="2";
        }
        else if(getIntent().getIntExtra("type",4)==3) {
            info[2]="3";
        }
        else if(getIntent().getIntExtra("type",4)==4) {
            info[2]="4";
        }
        else if(getIntent().getIntExtra("type",5)==5) {
            info[2]="5";
        }
        else if(getIntent().getIntExtra("type",6)==6) {
            info[2]="6";
        }
        else if(getIntent().getIntExtra("type",6)==7) {
            info[2]="7";
        }
        loadDecks load=new loadDecks();
        load.execute(info);
    }

    public void LoadDeckList (View v)
    {
        Deck t=(Deck)v.getTag();
        int id=t.getID();
        Intent intent = new Intent(this, List.class);
        intent.putExtra("id",id);
        intent.putExtra("type", getIntent().getIntExtra("type",0));
        startActivity(intent);
    }
    class loadDecks extends AsyncTask<String,Void,ArrayList>
    {

        protected void onPreExecute ()
        {
            TextView tv=new TextView(Table.getContext());
            tv.setText("Loading Decks");
            Table.addView(tv);


        }
        @Override
        protected ArrayList doInBackground(String...params)
        {

            ArrayList<Deck> DeckList=new ArrayList<Deck>();

            if(params[2].equals("1")) {
                DeckList=ServiceConnect.Get().GetScgDecks(params[0], params[1], "scg");
            }
            else if(params[2].equals("2")) {
                DeckList=ServiceConnect.Get().GetScgDecks(params[0], params[1], "scgLegacy");
            }
            else if(params[2].equals("3")) {
                DeckList=ServiceConnect.Get().GetScgDecks(params[0], params[1], "scgModern");
            }
            else if(params[2].equals("4")) {
                DeckList=ServiceConnect.Get().GetProDecks(params[0], params[1]);
            }
            else if(params[2].equals("5")) {
                DeckList=ServiceConnect.Get().GetScgDecks(params[0], params[1], "scgStandardClassic");
            }
            else if(params[2].equals("6")) {
                DeckList=ServiceConnect.Get().GetScgDecks(params[0], params[1], "scgModernClassic");
            }
            else if(params[2].equals("7")) {
                DeckList=ServiceConnect.Get().GetScgDecks(params[0], params[1], "scgLegacyClassic");
            }


            return DeckList;
        }

        protected void onPostExecute(ArrayList result)
        {
            Table.removeAllViews();
            for(int i=0;i<result.size();i++)
            {
                View DeckBox=inflater.inflate(R.layout.deck_box, Table, false);
                final Deck t=(Deck) result.get(i);
                final TextView Finish=(TextView) DeckBox.findViewById(R.id.Finish);
                final TextView Type=(TextView) DeckBox.findViewById(R.id.Type);
                final TextView Player=(TextView) DeckBox.findViewById(R.id.PlayerName);

                Finish.setText((t.getPlace()+""));
                Type.setText((t.getName()));
                Player.setText((t.getPlayer()));

                DeckBox.setTag(t);


                DeckBox.setOnClickListener(new View.OnClickListener(){
                    public void onClick(View arg0) {

                        LoadDeckList(arg0);

                    }
                });

                DeckBox.setOnTouchListener(new View.OnTouchListener() {

                    @Override
                    public boolean onTouch(View v, MotionEvent event) {

                        switch(event.getAction()) {

                            case MotionEvent.ACTION_DOWN:
                                v.setBackgroundColor(Color.LTGRAY);

                                break;
                            case MotionEvent.ACTION_CANCEL:

                                v.setBackgroundColor(Color.TRANSPARENT);
                                break;
                            case MotionEvent.ACTION_UP:

                                v.setBackgroundColor(Color.TRANSPARENT);
                                break;
                        }

                        return false;
                    }

                });


                Table.addView(DeckBox);
            }

        }
    }

}



