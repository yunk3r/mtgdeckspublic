package com.example.michaelyunker.mtgdecksearch;

/**
 * Created by michael.yunker on 5/10/2015.
 */
public class Deck {

    private int id;
    private String name;
    private String date;
    private String location;
    private String place;
    private String player;
    private String event;

    public Deck(int i, String n, String d, String l, String p, String pl, String e) {
        id = i;
        name = n;
        date = d;
        location = l;
        place = p;
        player = pl;
        event = e;
    }

    public Deck()
    {

    }

    public int getID() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDate() {
        return date;
    }

    public String getLocation() {
        return location;
    }

    public String getPlace() {
        return place;
    }

    public String getPlayer() {
        return player;
    }

    public String getEvent() {
        return event;
    }

}
