package com.example.michaelyunker.mtgdecksearch;

import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class SearchResults extends Activity {


    LinearLayout Table;
    LayoutInflater inflater;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_results);
        Table= (LinearLayout) findViewById(R.id.Results);
        inflater = this.getLayoutInflater();

        new loadSearchDecks().execute(getIntent().getStringExtra("format"), getIntent().getStringExtra("name"), getIntent().getStringExtra("cards"));


    }

    public void LoadDeckList (View v)
    {
        Deck t=(Deck)v.getTag();
        int id=t.getID();
        String type=t.getEvent();
        Intent intent = new Intent(this, List.class);
        intent.putExtra("id",id);
        Log.i("tag",type);
        if(type.equalsIgnoreCase("Scg Standard Open"))
        {
            intent.putExtra("type",1);
        }
        else if(type.equalsIgnoreCase("Scg Legacy Open"))
        {
            intent.putExtra("type",2);
        }
        else if(type.equalsIgnoreCase("Scg Modern Open"))
        {
            intent.putExtra("type",3);
        }
        else if(type.equalsIgnoreCase("Pro Standard"))
        {
            intent.putExtra("type",4);
        }
        else if(type.equalsIgnoreCase("Scg Standard Classic"))
        {
            intent.putExtra("type",5);
        }
        else if(type.equalsIgnoreCase("Scg Modern Classic"))
        {
            intent.putExtra("type",6);
        }
        else if(type.equalsIgnoreCase("Scg Legacy Classic"))
        {
            intent.putExtra("type",7);
        }
        startActivity(intent);
    }

    class loadSearchDecks extends AsyncTask<String,Void,ArrayList>
    {

        protected void onPreExecute ()
        {
            TextView tv=new TextView(Table.getContext());
            tv.setText("Loading Decks");
            Table.addView(tv);


        }
        @Override
        protected ArrayList doInBackground(String...params)
        {

            ArrayList<Deck> DeckList=new ArrayList<Deck>();

             DeckList=ServiceConnect.Get().Search(params[0], params[1], params[2]);


            return DeckList;
        }

        protected void onPostExecute(ArrayList result)
        {
            Table.removeAllViews();
            for(int i=0;i<result.size();i++)
            {
                View DeckBox=inflater.inflate(R.layout.deck_box, Table, false);
                final Deck t=(Deck) result.get(i);
                final TextView Finish=(TextView) DeckBox.findViewById(R.id.Finish);
                final TextView Type=(TextView) DeckBox.findViewById(R.id.Type);
                final TextView Player=(TextView) DeckBox.findViewById(R.id.PlayerName);

                Finish.setText((t.getPlace()+""));
                Type.setText((t.getName()+" - "+ t.getEvent()));
                Player.setText((t.getPlayer()+" "+t.getDate()+" "+t.getLocation()));

                DeckBox.setTag(t);


                DeckBox.setOnClickListener(new View.OnClickListener(){
                    public void onClick(View arg0) {

                        LoadDeckList(arg0);

                    }
                });

                DeckBox.setOnTouchListener(new View.OnTouchListener() {

                    @Override
                    public boolean onTouch(View v, MotionEvent event) {

                        switch(event.getAction()) {

                            case MotionEvent.ACTION_DOWN:
                                v.setBackgroundColor(Color.LTGRAY);

                                break;
                            case MotionEvent.ACTION_CANCEL:

                                v.setBackgroundColor(Color.TRANSPARENT);
                                break;
                            case MotionEvent.ACTION_UP:

                                v.setBackgroundColor(Color.TRANSPARENT);
                                break;


                        }

                        return false;
                    }

                });


                Table.addView(DeckBox);
            }

        }
    }

}
