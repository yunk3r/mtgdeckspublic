package com.example.michaelyunker.mtgdecksearch;


import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;



public class EventFragment extends Fragment {

    View view;
    public View onCreateView(LayoutInflater inflater,ViewGroup container, Bundle args) {
        view = inflater.inflate(R.layout.fragment_event, container, false);

        view.findViewById(R.id.scgStandard).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView tv=(TextView)v;
                Intent intent = new Intent(getActivity(), EventsActivity.class);
                intent.putExtra("type",1);
                startActivity(intent);

             }


    } );

        view.findViewById(R.id.scgStandard).setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {

                    case MotionEvent.ACTION_DOWN:
                        v.setBackgroundColor(Color.LTGRAY);

                        break;
                    case MotionEvent.ACTION_CANCEL:

                        v.setBackgroundColor(Color.TRANSPARENT);
                        break;
                    case MotionEvent.ACTION_UP:

                        v.setBackgroundColor(Color.TRANSPARENT);
                        break;
                }

                return false;
            }

        });


        view.findViewById(R.id.scgStandardClassic).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView tv = (TextView) v;
                Intent intent = new Intent(getActivity(), EventsActivity.class);
                intent.putExtra("type", 5);
                startActivity(intent);
            }

        });

        view.findViewById(R.id.scgStandardClassic).setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {

                    case MotionEvent.ACTION_DOWN:
                        v.setBackgroundColor(Color.LTGRAY);

                        break;
                    case MotionEvent.ACTION_CANCEL:

                        v.setBackgroundColor(Color.TRANSPARENT);
                        break;
                    case MotionEvent.ACTION_UP:

                        v.setBackgroundColor(Color.TRANSPARENT);
                        break;
                }

                return false;
            }

        });

        view.findViewById(R.id.scgLegacy).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView tv = (TextView) v;
                Intent intent = new Intent(getActivity(), EventsActivity.class);
                intent.putExtra("type", 2);
                startActivity(intent);
            }

        });

        view.findViewById(R.id.scgLegacy).setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {

                    case MotionEvent.ACTION_DOWN:
                        v.setBackgroundColor(Color.LTGRAY);

                        break;
                    case MotionEvent.ACTION_CANCEL:

                        v.setBackgroundColor(Color.TRANSPARENT);
                        break;
                    case MotionEvent.ACTION_UP:

                        v.setBackgroundColor(Color.TRANSPARENT);
                        break;
                }

                return false;
            }

        });

        view.findViewById(R.id.scgLegacyClassic).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView tv = (TextView) v;
                Intent intent = new Intent(getActivity(), EventsActivity.class);
                intent.putExtra("type", 7);
                startActivity(intent);
            }

        });

        view.findViewById(R.id.scgLegacyClassic).setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {

                    case MotionEvent.ACTION_DOWN:
                        v.setBackgroundColor(Color.LTGRAY);

                        break;
                    case MotionEvent.ACTION_CANCEL:

                        v.setBackgroundColor(Color.TRANSPARENT);
                        break;
                    case MotionEvent.ACTION_UP:

                        v.setBackgroundColor(Color.TRANSPARENT);
                        break;
                }

                return false;
            }

        });

        view.findViewById(R.id.scgModern).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView tv = (TextView) v;
                Intent intent = new Intent(getActivity(), EventsActivity.class);
                intent.putExtra("type", 3);
                startActivity(intent);
            }

        });

        view.findViewById(R.id.scgModern).setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {

                    case MotionEvent.ACTION_DOWN:
                        v.setBackgroundColor(Color.LTGRAY);

                        break;
                    case MotionEvent.ACTION_CANCEL:

                        v.setBackgroundColor(Color.TRANSPARENT);
                        break;
                    case MotionEvent.ACTION_UP:

                        v.setBackgroundColor(Color.TRANSPARENT);
                        break;
                }

                return false;
            }

        });

        view.findViewById(R.id.scgModernClassic).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView tv = (TextView) v;
                Intent intent = new Intent(getActivity(), EventsActivity.class);
                intent.putExtra("type", 6);
                startActivity(intent);
            }

        });

        view.findViewById(R.id.scgModernClassic).setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {

                    case MotionEvent.ACTION_DOWN:
                        v.setBackgroundColor(Color.LTGRAY);

                        break;
                    case MotionEvent.ACTION_CANCEL:

                        v.setBackgroundColor(Color.TRANSPARENT);
                        break;
                    case MotionEvent.ACTION_UP:

                        v.setBackgroundColor(Color.TRANSPARENT);
                        break;
                }

                return false;
            }

        });

        view.findViewById(R.id.ProStandard).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView tv = (TextView) v;
                Intent intent = new Intent(getActivity(), EventsActivity.class);
                intent.putExtra("type", 4);
                startActivity(intent);
            }
        });

        view.findViewById(R.id.ProStandard).setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {

                    case MotionEvent.ACTION_DOWN:
                        v.setBackgroundColor(Color.LTGRAY);

                        break;
                    case MotionEvent.ACTION_CANCEL:

                        v.setBackgroundColor(Color.TRANSPARENT);
                        break;
                    case MotionEvent.ACTION_UP:

                        v.setBackgroundColor(Color.TRANSPARENT);
                        break;
                }

                return false;
            }

        });


        return view;
    }

}
